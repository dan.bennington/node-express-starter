# Express Starter

## Running the application

To run the application in development, run:
`npm run dev`

To run the application in production, first run:
`npm run build`
Then:
`npm start`
